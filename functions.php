<?php
	
	function mywork_functions(){
		// Register Custom Navigation Walker
		require_once get_template_directory() . '/wp-bootstrap-navwalker.php';


		//title
		add_theme_support('title-tag');

		//thumbnails
	add_theme_support('post-thumbnails');

		//logo
		add_theme_support('custom-logo', array(
			'default' => get_template_directory_uri().'/img/logo.png',
		));


		if(function_exists('register_nav_menus')){//menu
		register_nav_menus(array(
			'primarymenu' => 'Header Menu',
			'secondarymenu' => 'Footer Menu',
		));

	}
		

	//read more
		function read_more($limit){
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content, 0, $limit);

		echo implode(" ", $less_content);
	}


	


	}
	add_action('after_setup_theme','mywork_functions')

?>