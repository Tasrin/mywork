<?php
get_header();
?>

    <!-- slide  stare -->
    <?php
        $slider = new WP_Query(array(
            'post_type'=>'post',
            'category_name'=>'slider',
            'posts_per_page'=>2,
            'order'=>'ASC',
        ));
        $count=0;
    ?>

    <div class="slide">
        <div class="container">
            <!--<img src="<?php //echo esc_url(get_template_directory_uri());?>/img/work.jpg" width="100%">-->

            <div class="bd-example">
              <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">

                <?php while($slider->have_posts()): $slider->the_post();?>
                  <div class="carousel-item <?php echo ($count==0)? 'active' : '';?>">
                    <?php the_post_thumbnail();?>
                    <div class="carousel-caption d-none d-md-block">
                      <h5><?php the_title();?></h5>
                      <p><?php the_content();?></p>
                    </div>
                  </div>

                <?php
                $count++;
                 endwhile; ?>
                 <!-- <div class="carousel-item">
                    <img src="<?php //echo esc_url(get_template_directory_uri());?>/img/header-bg.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Second slide label</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                  -->
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>

        </div>
    </div>

    <!-- slide End -->

    <div class="content">
        <div class="container">
            <div class="row bg">
            <?php 
                $wpost = new WP_Query(array(
                    'post_type' => 'post',
                    'category_name' => 'mywork-post',
                    'posts_per_page' => 2,
                    'order' => 'DESC',
                )); 
            ?>

            <?php while($wpost->have_posts()) : $wpost->the_post(); ?>
                <div class="col-md-6" >
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p class="text-justify">
                        <?php read_more(13); ?><a href="<?php the_permalink(); ?>">...Read More</a>
                    </p>
                </div>
            <?php endwhile; ?>
            </div>
            </div>
        </div>




<?php
get_footer();
?>