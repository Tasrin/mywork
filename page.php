<?php 
	get_header();
?>
	
<div class="single">
	<div class="container">
		<?php while(have_posts()) : the_post(); ?>
		<div class="wrap-about py-5 ftco-animate">
	        <div class="heading-section mb-5">
	            <h2 class="mb-4"><?php the_title(); ?></h2>
	        </div>
	        <div class="">
	        	<div class="simg">
	        		<?php the_post_thumbnail(); ?>
	        	</div>
				<p class="text-justify">
					<?php the_content(); ?>
				</p>
			</div>
		</div>
	<?php endwhile;  ?>
	</div>
</div>

<?php 
	get_footer();
?>