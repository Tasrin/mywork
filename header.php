<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    
     <meta charset="<?php bloginfo('charset');?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?>/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?>/style.css">
    <script src="<?php echo esc_url(get_template_directory_uri());?>/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri());?>/bootstrap/js/bootstrap.min.js"></script>
    <?php wp_head(); ?>
</head>
<body>
    <div class="top">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
                <div class="container">
                <!-- <a href="#">
                     <img src="<?php //echo esc_url(get_template_directory_uri());?>/img/logo.png" class="img img-responsive logo" alt="Logo">
                   
                 </a>
             -->

                   <?php 
                         if(function_exists('the_custom_logo')){
                            the_custom_logo();
                         }
                     ?>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            wp_nav_menu(array(
                               'theme_location'  => 'primarymenu',
                                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id'    => 'navbarResponsive',
                                'menu_class'      => 'navbar-nav ml-auto',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'          => new WP_Bootstrap_Navwalker(),
                            ));
                        }
                    ?>
                    <!--<div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item "><a  class="nav-link" href="about.php">About Us</a></li>
                            <li class="nav-item "><a class="nav-link" href="service.php">Service</a></li>
                            <li class="nav-item "><a class="nav-link" href="contact.php">Contact Us</a></li>
                        </ul>
                    </div>-->
                </div>
            </nav>
        </div>
    </div>

